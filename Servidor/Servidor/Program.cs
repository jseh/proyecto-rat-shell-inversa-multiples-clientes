﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Threading.Tasks;

namespace Servidor
{
    class Program
    {
        static async Task Main(string[] args)
        {
            // Ok
            var clientes = new List<TcpClient>();
            TcpListener listener = new TcpListener(IPAddress.Parse("127.0.0.1"), 5555);
            Console.WriteLine("Inicializando puerto");
            listener.Start();
            Console.WriteLine("Escuchando por conexión...");
            await Task.WhenAny(MostrarMenu(clientes),AceptarConexiones(clientes, listener));
          
        }

        static async Task MostrarMenu(List<TcpClient> clientes)
        {

            await Task.Run(() => {

                var MantenerMenuPrincipal = true;
                while (MantenerMenuPrincipal)
                {
                    Console.WriteLine("Main>>");
                    var opcion = Console.ReadLine();
                    switch (opcion)
                    {
                        default:
                            string[] opciones = opcion.Split(' ');
                            try
                            {
                                if (opciones[0] == "i" && opciones[1].All(Char.IsNumber))
                                {
                                    if (clientes.Count > Convert.ToInt32(opciones[1]))
                                    {
                                        InteractuarCon(Convert.ToInt32(opciones[1]), clientes);
                                    }
                                }
                            }catch (IndexOutOfRangeException) {

                            }
                            break;
                        case "clientes":
                            if(clientes.Count == 0)
                            {
                                Console.WriteLine("No clientes conectados");
                            }
                            else
                            {
                                foreach (var cliente in clientes)
                                {                                    
                                    var direccion = ((IPEndPoint)cliente.Client.RemoteEndPoint).Address.ToString();
                                    var id = clientes.IndexOf(cliente);
                                    Console.WriteLine($"{id}-{direccion}");                                   
                                }
                            }
                            break;
                        case "salir":
                            MantenerMenuPrincipal = false;
                            break;                  
                    }
                }

            });
            
        }

        static void InteractuarCon(int id, List<TcpClient> clientes)
        {


            NetworkStream stream = clientes[id].GetStream();

            BinaryWriter mBinaryWriter = new BinaryWriter(stream);
            BinaryReader mBinaryReader = new BinaryReader(stream);

            while (true)
            {
                Console.WriteLine(">>");
                var comando = Console.ReadLine();
                if (comando == "salir")
                {
                    break;
                }

               
                // enviar
                try
                {
                    mBinaryWriter.Write(comando);
                }
                catch (System.IO.IOException)
                {
                    Console.WriteLine("Error al escribir");
                    clientes.RemoveAt(id);
                    break;
                }

                // recibir                                                    
                Console.WriteLine(mBinaryReader.ReadString());
                                       
            }


            // no se pueden cerrar porque tambien cierran la conexion
            //mBinaryReader.Close();
            //mBinaryWriter.Close();
            //stream.Close();
            
            //catch(SocketException se)
            //{
            //    Console.WriteLine("Conexion Fallida");
            //}
            //catch (Exception ex)
            //{
            //    Console.WriteLine(ex.ToString());
            //    Console.ReadKey();
            //}
            //finally
            //{
            //    //Esto siempre se ejecuta, ya sea cuando ocurra una excepcion 
            //    //o cuando finalize todo lo que hay el bloque try.

            //    //listener.Stop();
            //    Console.WriteLine("Bloque finally");
            //}


        }

        static async Task AceptarConexiones(List<TcpClient> clientes, TcpListener listener)
        {
            while (true)
            {
                clientes.Add( await listener.AcceptTcpClientAsync() );
                Console.WriteLine("Conexion Aceptada.");
            }
            

        }
    }
}
